import openmesh as om
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import mpl_toolkits.mplot3d as m3d

from matplotlib.tri import Triangulation as Tri
import json
import sys
import numpy.linalg as alg
from mpl_toolkits.mplot3d import proj3d
import sympy as sp
import sympy.plotting as spplot
import math
from functools import reduce
import sympy.functions as spf
from scipy.spatial import Delaunay as de
from pygmsh.opencascade.geometry import Geometry as cad
from pygmsh.opencascade.surface_base import SurfaceBase as sur
from openmesh import TriMesh


def testDe():
    pts=np.random.rand(88,2)
    tri=de(pts)
    
    fig, axs=plt.subplots(1,2)
    ax1,ax2=axs
    ax1.scatter(pts[:,0], pts[:,1])
    ax2.triplot(pts[:,0], pts[:,1], tri.simplices)
    fig.show()

def testDe2():
    pts=[np.random.rand(88,2) for i in range(2)]
    tris=[de(pts[i]) for i in range(2)]
    fig, axs=plt.subplots(1,2)
    [axs[i].triplot(pts[i][:,0], pts[i][:,1], tris[i].simplices) for i in range(2)]
    fig.show()
    return tris

def testMesh():
    
    pts, part_a, part_b=Hammer(dense=0.05).get_mesh()

    tri, ptsIdx, ppts=part_a

    faces=[ptsIdx[x] for x in tri.simplices]
    #faces=tri.simplices
    tri, ptsIdx, ppts=part_b
    faceb=[ptsIdx[x] for x in tri.simplices]
    faceb=[reversed(x) for x in faceb]
    faceb=[np.array([y for y in x]) for x in faceb]
    faces.extend(faceb)
    faces=np.array(faces)
    
    mesh = TriMesh()
    #vhs=np.array([mesh.add_vertex(x) for x in pts[ptsIdx]])
    vhs=np.array([mesh.add_vertex(x) for x in pts])
    fhs=np.array([mesh.add_face(vhs[x]) for x in faces])

    '''
    return pts

    mesh = TriMesh()
    vhs=np.array([mesh.add_vertex(x) for x in pts])
    fhs=np.array([mesh.add_face(vhs[x]) for x in tri.simplices])

    vh0=mesh.vertex_handle(0)
    vhs=[ vh for vh in om.VertexIter(mesh, vh0)]
    bvs=filter(lambda x:mesh.is_boundary(x), vhs)
    bvhs=[v for v in bvs]
    bv_idx=[ x.idx() for x in bvhs]
    bv_idx=np.unique(tri.convex_hull)
    boundary_pts=mesh.points()[bv_idx]
    #return boundary_pts
    
    #for vh in om.VertexIter(mesh, vh):
    '''
        
        
    '''
    from  testom import BoundaryTriangleMesh as BM
    bm=BM()
    mesh=bm.mesh
    '''

    
    
    import mayavi.mlab as mlab
    pts=mesh.points()
    mlab.triangular_mesh(pts[:,0], pts[:,1], pts[:,2], mesh.face_vertex_indices(), representation='wireframe')
    om.write_mesh('hammer.obj', mesh)
    #pts=boundary_pts
    #mlab.points3d(pts[:,0], pts[:,1], pts[:,2], mode='cone', scale_factor=0.1)
    #mlab.plot3d(pts[:,0], pts[:,1], pts[:,2])
    mlab.show()
    
    return mesh


class Hammer():

    def __init__(self, r_handle=1.3, len_handle=4., w_head_middle=3., len_head_middle=3., len_head_end=3., w_head_end=3., dense=0.1):
        
        self.r_handle, self.len_handle= r_handle, len_handle
        self.w_head_middle, self.len_head_middle=w_head_middle, len_head_middle
        self.len_head_end, self.w_head_end= len_head_end, w_head_end
        self.dense=dense
    
    def head_end(self, gap=0.02): #the octagon part
        w, x_start=self.w_head_end, self.len_head_middle/2.,
        x_end=x_start+ self.len_head_end
        dense=self.dense

        rw=np.linspace((0.-w/2)*(1+gap) , (1-gap)*w/2., w/dense)
        face_btm=[ np.array([x_end, y,z]) for y in rw for z in rw]
        face_btm=np.array([p for p in filter(lambda p: (1+gap)*(abs(p[1])+abs(p[2]))<w*math.sqrt(2)/2, face_btm)])
        face_connect=[ np.array([x_start, y,z]) for y in rw for z in rw]
        face_connect=np.array([p for p in filter(lambda p: abs(p[1])+abs(p[2])>(1+gap)*w*math.sqrt(2)/2, face_connect)])

        t=w*math.sqrt(2)/4
        ry=np.linspace((0-t/2)*(1-gap), (1-gap)*t/2, t/dense)
        #ry1=np.linspace((0-t/2)*(1-gap), 0, t/dense/2)
        #ry2=np.linspace(0, (1-gap)*t/2, t/dense/2)
        #ry=np.unique(np.hstack((ry1, ry2)))
        rx=np.linspace(x_start*(1+gap), x_end*(1-gap), (x_end-x_start)/dense)
        side=np.array([ np.array([x, y, w/2]) for x in rx for y in ry])
        return face_btm, face_connect, side

    def head_end_allside(self):
        '''
        side order: front(y+), right(x+), back(y-), left(x-), top(z+), bottom(z-)
        '''
        face_btm, face_connect, face_side=self.head_end()
        angles=[x*math.pi/4 for x in range(1,8)]
        co=[np.mat([ [1,0,0],
                     [0,math.cos(a), math.sin(a)],
                     [0,-math.sin(a), math.cos(a)]
        ]) for a in angles]

        face_otherside=[ np.array((k*face_side.transpose()).transpose())  for k in co ]
        sides=[]
        sides.append(face_otherside[1])#front(y+) 101
        sides.append(face_btm) #right (x+) 011
        sides.append(face_otherside[5])#back(y-) 101
        sides.append(face_connect) #left(x-) 011
        sides.append(face_otherside[0]) #front,top (y+, z+) 101
        sides.append(face_side) # top (z+) 110
        sides.append(face_otherside[6]) #top, back (y-, z+) 101
        sides.append(face_otherside[4]) #101
        sides.append(face_otherside[3]) #bottom 110
        sides.append(face_otherside[2]) #front bottom, 110
        mask='''101,011,101,011,
        101,110,101,101,110,110'''
        mask=[x.strip() for x in mask.replace('\n','').split(',')]
        #print (mask)
        mask=[[int(x) for x in y] for y in mask]

        return sides ,mask

    def head_end_opposite(self, head_end):
        s=[]
        for side in head_end:
            ss=np.array([ x*np.array([-1,1,1]) for x in side ])
            s.append(ss)
        ss=s[1]
        s[1]=s[3]
        s[3]=ss
        return s

    
    def head_middle(self, gap=0.02):
        w, x_start, x_end=self.w_head_middle,-self.len_head_middle/2, self.len_head_middle/2
        r=self.r_handle
        dense=self.dense
        
        rw=np.linspace((-w/2.)*(1-gap), (1-gap)*w/2., w/dense)
        ry=rw
        rx=np.linspace(x_start*(1-gap), x_end*(1-gap), (x_end-x_start)/dense)
        # this is the top side, require to cut the middle circle
        side=np.array([ np.array([x, y, (1+gap)*w/2]) for x in rx for y in ry])
        
        #return side
        angles=[x*math.pi/2 for x in range(1,4)]
        co=[np.mat([ [1,0,0],
                     [0,math.cos(a), -math.sin(a)],
                     [0,math.sin(a), math.cos(a)]
        ]) for a in angles]

        otherside=[ np.array((k*side.transpose()).transpose())  for k in co ]

        side=filter(lambda x: (x[0]*x[0]+x[1]*x[1])>=(1+2*gap)*r*r, side)
        side=np.array([ np.array(x) for x in side]) # cut the middle circle
        sides=[]
        sides.append(otherside[2]) # front 
        #sides.append(None) # x+
        sides.append(otherside[0]) #  back
        #sides.append(None) # x-
        sides.append(side) #top
        sides.append(otherside[1]) # bottom
        mask=[[int(y) for y in x ] for x in '101,101,110,110'.split(',')]
        #print( mask)
        return sides, mask
    

    def cylinder_handle(self, gap=0.03):
        z_start= self.w_head_middle/2
        z_end=z_start+self.len_handle
        r=self.r_handle
        dense=self.dense
        
        ang_num=100
        a_range=np.linspace(0, math.pi*2, ang_num)
        z_range=np.linspace(z_start*(1+2*gap), z_end*(1-gap), (z_end-z_start)/dense)
        side=np.array([ np.array([r*math.cos(a), r*math.sin(a), z]) for z in z_range for a in a_range ])
        xy_range=np.linspace(-r, r, (z_end-z_start)/dense)
        #handle bottom
        btm=[[ x, y, z_end]  for x in xy_range for y in xy_range ] #create a square
        btm=filter(lambda x: (1+2*gap)*(x[0]*x[0]+x[1]*x[1])<r*r, btm) # only keep the points within the circle
        btm=np.array([np.array(x) for x in btm])
        return [side, btm]
        
    def get_sides(self):
        sides_head_end, _m=self.head_end_allside()
        sides_head_middle, _m=self.head_middle()
        cylinder=self.cylinder_handle()
        sides_head_opposite=self.head_end_opposite(sides_head_end)
        sides=[]
        sides.extend(sides_head_end)
        sides.extend(sides_head_middle)
        sides.extend(cylinder)
        sides.extend(sides_head_opposite)
        return sides
        
    def get_mesh(self):
        def proj(p0, p, z):
            x0,y0,z0=p0
            x1,y1,z1=p
            return np.array([x0-(x0-x1)*(z0-z)/(z0-z1),
                             y0-(y0-y1)*(z0-z)/(z0-z1)])


        sides=self.get_sides()
        pts=[]
        for s in sides:
            pts.extend(s)
        pts=np.array(pts)
        pts=np.unique(pts, axis=0)
            
        ptsIdx_a=filter(lambda x: pts[x][2]<=0, range(len(pts)))
        ptsIdx_a=np.array([x for x in ptsIdx_a])
        pts_a=pts[ptsIdx_a]
        p0=(0,0,5.)
        z=-10.
        ppts_a=np.array([np.array(proj(p0, p, z)) for p in pts_a])
        tri_a=de(ppts_a, incremental=True)
        part_a=(tri_a, ptsIdx_a, ppts_a)


        borderIdx=np.unique(tri_a.convex_hull)
        borderIdx=ptsIdx_a[borderIdx]

        ptsIdx_b=filter(lambda x: pts[x][2]>0, range(len(pts)))
        ptsIdx_b=np.array([x for x in ptsIdx_b])
        ptsIdx_b=np.concatenate((ptsIdx_b, borderIdx), axis=0)
        pts_b=pts[ptsIdx_b]
        #return pts_b, borderPts

        p0=(0,0,-5.)
        z=10.
        ppts_b=np.array([np.array(proj(p0, p, z)) for p in pts_b])
        tri_b=de(ppts_b, incremental=True)
        part_b=(tri_b, ptsIdx_b, ppts_b)

        

        
        return pts, part_a, part_b

        '''
        fig, axs=plt.subplots(1,2)
        ax1,ax2=axs

        tri, pts, ppts=part_b
        ax1.scatter(ppts[:,0], pts[:,1])
        ax2.triplot(ppts[:,0], pts[:,1], tri.simplices)
        fig.show()

        return tri, pts
        '''
        '''
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.plot_trisurf(pts[:,0], pts[:,1], pts[:,2], triangles=tri.simplices, cmap=plt.cm.Spectral)
        fig.show()

        return (tri_a, pts_a), (tri_b, pts_b)
        
        #return tri ,pts
        '''

        import mayavi.mlab as mlab
        #mlab.points3d(pts[:,0], pts[:,1], pts[:,2], opacity=1., mode='cylinder', scale_factor=0.1,colormap="copper", color=tuple(np.random.random(3)))
        #mlab.points3d(ppts[:,0], ppts[:,1], z*np.ones(len(ppts)), opacity=1., mode='cylinder', scale_factor=0.1,colormap="copper", color=tuple(np.random.random(3)))
        pts=pts_a
        #mlab.triangular_mesh(pts[:,0],pts[:,1], pts[:,2], tri.simplices)
        
        rpt='fancymesh'
        rpt='points'
        rpt='surface'
        rpt='wireframe'
        mlab.triangular_mesh(pts[:,0],pts[:,1], pts[:,2], tri_a.simplices, representation=rpt)
        pts=pts_b
        mlab.triangular_mesh(pts[:,0],pts[:,1], pts[:,2], tri_b.simplices, representation=rpt)
    
        mlab.show()

        return pts


    def plot_pt(self, sides=None, use_mlab=True):
    #def plot(self, sides=None, use_mlab=False):
        if None==sides:
            sides=self.get_sides()
            
        print(len(sides))
        if use_mlab:
            import mayavi.mlab as mlab
            mlab.figure(size=(600, 500))            
            for side in sides:
                if side is None:
                    continue
                pts=[[],[],[]]
                for i in range(3):
                    pts[i].extend(side[:,i])
                mlab.points3d(pts[0], pts[1], pts[2], opacity=1., mode='cylinder', scale_factor=0.1,colormap="copper", color=tuple(np.random.random(3)))

                #mlab.points3d(side[:,0], side[:,1], side[:,2], opacity=0.5#, c='b')
                #mlab.plot3d(side[:,0], side[:,1], side[:,2], opacity=0.5, colormap="copper", color=tuple(np.random.random(3)))
                #mlab.points3d(side[:,0], side[:,1], side[:,2], opacity=0.5, colormap="copper", color=tuple(np.random.random(3)))
            #mlab.plot3d(pts[0], pts[1], pts[2], opacity=0.9, colormap="copper", color=tuple(np.random.random(3)))
            #mlab.plot3d(pts[0], pts[1], pts[2], opacity=0.9, color=tuple(np.random.random(3)))
            #mlab.points3d(pts[0], pts[1], pts[2], opacity=1.0, colormap="copper", color=tuple(np.random.random(3)))

            mlab.view(azimuth=45)
            mlab.axes(x_axis_visibility=True, y_axis_visibility=True, z_axis_visibility=True)
            #mlab.show(stop=True)
            mlab.show()
            return

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        for side in sides:
            if side is None:
                continue
            ax.scatter(side[:,0], side[:,1], side[:,2])#, c='b')
        fig.show()

    def dump_points(self, filepath, sides=None):
        if None==sides:
            sides=self.get_all()
        lines=[]
        with open(filepath, 'w') as f:
            for side in sides:
                for pt in side:
                    line=' '.join(['{}'.format(x) for x in pt])+'\n'
                    lines.append(line)
            f.writelines(lines)
            
    def get_points(self, sides=None):
        if None==sides:
            sides=self.get_all()

        pts=[]
        for side in sides:
            pts.extend(side)
                
        return np.array(pts)



    def plot_mesh(self, tri=None, use_mlab=True):
    #def plot(self, sides=None, use_mlab=False):
        if None==tri:
            tri=self.get_mesh()
            
        if use_mlab:
            import mayavi.mlab as mlab
            mlab.triangular_mesh(tri.points[:,0], tri.points[:,1], tri.points[:,2], triangles=tri.simplices  )
            return

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.plot_trisurf(tri.points[:,0], tri.points[:,1], tri.points[:,2], triangles=tri.simplices, cmap=plt.cm.Spectral)
        fig.show()

        
            

if __name__=='__main__':
    h=Hammer()
    #h.plot()

        

    
        

        
        

        
                
