import openmesh as om
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import mpl_toolkits.mplot3d as m3d

from matplotlib.tri import Triangulation as Tri
import json
import sys
import numpy.linalg as alg
from mpl_toolkits.mplot3d import proj3d
import sympy as sp
import sympy.plotting as spplot
import math
from functools import reduce
import sympy.functions as spf
from scipy.spatial import Delaunay as de
from pygmsh.opencascade.geometry import Geometry as cad
from pygmsh.opencascade.surface_base import SurfaceBase as sur
from openmesh import TriMesh
from h import Hammer

import pygalmesh as pm

def getHammer():
    '''
    return all points (3d scan result)
    and tetrahedron
    '''
    h=Hammer(len_handle=4, dense=0.3)
    sides_head_end, _m=h.head_end_allside()
    sides_head_middle, _m=h.head_middle()
    cylinder=h.cylinder_handle()
    sides_head_opposite=h.head_end_opposite(sides_head_end)
    
    sides_head=[]
    for x in [sides_head_end, sides_head_middle, sides_head_opposite]:
        sides_head.extend(x)

    s=sides_head_end.pop(3)
    sides_head_middle.append(s)
    s=sides_head_opposite.pop(1)
    sides_head_middle.append(s)

    ptss=[]
    tes=[]
    for part in [sides_head_end, sides_head_middle, cylinder,sides_head_opposite]:
        pts=[]
        for x in part:
            pts.extend(x)
        te=de(pts)
        ptss.append([[p for p in x] for x in pts])
        tes.append(te)

    return ptss,tes


def getPartialHammer():
    pass

def getMissing():
    m=pm.generate_from_off(
             'hammerBoundaryCells.off',
             't.mesh',
             facet_angle=25.0,
             facet_size=0.15,
             facet_distance=0.008,
             cell_radius_edge_ratio=3.0,
             verbose=False
    )
    return m

def test1():
    radius = 1.0
    displacement = 0.5
    s0 = pm.Ball([displacement, 0, 0], radius)
    s1 = pm.Ball([-displacement, 0, 0], radius)
    u = pm.Difference(s0, s1)
    pm.generate_mesh(u, 'diff.mesh', cell_size=0.1, edge_size=0.1)
    import meshio as mi
    m=mi.read('diff.mesh')
    mi.write_points_cells('diff.vtk', m.points, {'tetra':m.cells['tetra']})
    return s0, s1, u

import meshio as mi
def test(ptss=None, tes=None):

    if None==ptss:
        ptss, tes=getHammer()
        
    tes=[x.simplices for  x in tes]
    pts=[]
    te=[]
    offset=0
    for i in range(len(ptss)):
        pts.extend(ptss[i])
        te.extend([ [y+offset for y in x] for x in tes[i]])
        offset=offset+len(ptss[i])
        
    te=np.array(te)
    pts=np.array(pts)
    
    mi.write_points_cells('hammerCells.vtk', pts, {'tetra':te})
    mi.write_points_cells('hammerCells.mesh', pts, {'tetra':te})
    m=mi.read('hammerVol.mesh')
    #m=pm.generate_from_off('hammerVol.off')
    return m

from itertools import combinations as cb
from itertools import permutations as pe
from collections import Counter
def extractSurface(m=None):
    m=test()
    cells=m.cells['tetra']
    pts=m.points
    cs=[ [f  for f in  cb(c,3)]  for c in cells]
    cs_=[]
    for c in cs:
        cc=[]
        for f in c:
            ff=list(f)
            ff.sort()
            cc.append('-'.join([str(n) for n in ff]))
        cs_.append(cc)
    
    faces=np.array(cs_).flatten()
    cnt=Counter(faces)
    bdFaces=list(filter(lambda x: 1==cnt[x], [x for x in cnt]))
    cs_=[set(x) for x in cs_]
    bdCells_idx=list( filter(lambda idx_cell: any(map(lambda f: f in cs_[idx_cell], bdFaces)), range(len(cs_)) ))
    
    bdCells=cells[bdCells_idx]
    
    mi.write_points_cells('hammerBoundaryCells.vtk', pts, {'tetra':bdCells})
    mi.write_points_cells('hammerBoundaryCells.mesh', pts, {'tetra':bdCells})
    
    fs=np.array([[int(y) for y in x.split('-')] for x in bdFaces])
    mi.write_points_cells('hammerBoundaryFaces.off', pts, {'triangle':fs})
    mi.write_points_cells('hammerBoundaryFaces.mesh', pts, {'triangle':fs})
    #mi.write_points_cells('hammerBoundaryFaces.obj', pts, {'triangle':fs})
    
    import mayavi.mlab as mlab
    rpt='wireframe'
    mlab.triangular_mesh(pts[:,0],pts[:,1], pts[:,2], fs, representation=rpt)

    return bdFaces, cs_, bdCells_idx

    for te in faces:
        for f in te:
            f.sort()
            s='-'.join([str(n) for n in f])
            fs.append(s)
    
    boundaries=[[int(n) for n in x.split('-')] for x in np.unique(fs)]
    
            
    fs=[]
    for b in boundaries:
        for cell in cells:
            if len(set(b)-set(cell))>0:
                continue
            
            d=list(set(cell)-set(b))
            c=[]
            c.extend(cell)
            c.remove(d[0])
            fs.append(c)
            break
            
    return fs
    
    return faces


def testShow(f=None):
    if None==f:
        test()
    import os
    from mayavi.core.api import Engine
    from mayavi.sources.vtk_file_reader import VTKFileReader
    from mayavi.modules.surface import Surface

    vtkFile = 'hammerCells.vtk'

    # Create the MayaVi engine and start it.
    engine = Engine()
    engine.start()
    scene = engine.new_scene()

    # Read in VTK file and add as source
    reader = VTKFileReader()
    reader.initialize(vtkFile)
    engine.add_source(reader)

    # Add Surface Module
    surface = Surface()
    engine.add_module(surface)

    # Move the camera
    scene.scene.camera.elevation(-70)

    # Save scene to image file
    #scene.scene.save_png('image.png')

    # Create a GUI instance and start the event loop.
    # This stops the window from closing
    from pyface.api import GUI
    gui = GUI()
    gui.start_event_loop()
    


